The cluster.yml file in the directory is the file used for the OKD cluster deployed.

In OKD, by default, ResourceCatalog isn't enabled by default for redhat registries. They can be enabled
by going to Settings > Clusters > Global Config > Yaml. There, we should change all the 'disabled: true' 
statements to 'disabled: false'.

To acknowledge the ability to upgrade from a 4.8 to a 4.9 OKD cluster:
`oc -n openshift-config patch cm admin-acks --patch '{"data":{"ack-4.8-kube-1.22-api-removals-in-4.9":"true"}}' --type=merge`

Install TektonCD via cli
`oc apply -f https://storage.googleapis.com/tekton-releases/operator/latest/release.yaml`

Install all the components:
`oc apply -f https://raw.githubusercontent.com/tektoncd/operator/main/config/crs/kubernetes/config/all/operator_v1alpha1_config_cr.yaml`

Install the Knative Operator with cli
`oc apply -f https://github.com/knative/operator/releases/latest/download/operator.yaml`

 #Istio

By default, OpenShift doesn’t allow containers running with user ID 1337. You must enable containers running with UID 1337 for Istio’s service accounts by running the command below. Make sure to replace istio-system if you are deploying Istio in another namespace:
`oc adm policy add-scc-to-group anyuid system:serviceaccounts:istio-system`

Install Istio in Openshift
`istioctl install --set profile=openshift`

After installation is complete, expose an OpenShift route for the ingress gateway.
`oc -n istio-system expose svc/istio-ingressgateway --port=http2`


#Security context constraints for application sidecars

The Istio sidecar injected into each application pod runs with user ID 1337, which is not allowed by default in OpenShift. To allow this user ID to be used, execute the following commands. Replace <target-namespace> with the appropriate namespace.
`oc adm policy add-scc-to-group anyuid system:serviceaccounts:<target-namespace>`

When removing your application, remove the permissions as follows.
`$ oc adm policy remove-scc-from-group anyuid system:serviceaccounts:<target-namespace>`

#Additional requirements for the application namespace

CNI on OpenShift is managed by Multus, and it requires a NetworkAttachmentDefinition to be present in the application namespace in order to invoke the istio-cni plugin. Execute the following commands. Replace <target-namespace> with the appropriate namespace.
`cat <<EOF | oc -n <target-namespace> create -f -
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: istio-cni
EOF`

When removing your application, remove the NetworkAttachmentDefinition as follows.
`oc -n <target-namespace> delete network-attachment-definition istio-cni`

#Random commands:

Get opaque secrets in one line:
`oc get secret credential-keycloak -n keycloak -o jsonpath="{.data.ADMIN_PASSWORD}" | base64 --decode`

List unmanaged operators:

`oc get clusterversion version -ojsonpath='{range .spec.overrides[*]}{.name}{"\n"}{end}' | nl -v 0`

See projects:

`oc projects`

Login with podman
`podman login -u kubeadmin -p $(oc whoami -t) --tls-verify=false $HOST`

