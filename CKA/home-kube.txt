# To add nodes
kubeadm join 192.168.0.85:6443 --token gu7u4x.8k5x2r0tbex7v0ci \
--discovery-token-ca-cert-hash sha256:1758786697116f0951c784bb97cb40c1e7a85bb0f988945201da4cdbee236957

# To get a bearer token
    kubectl get secret | grep cluster-admin-dashboard-sa
# Describe the secret generate in previous step
    kubectl describe secret <cluster-admin-dashboard-sa-token-name>
# Copy token and paste it in the dashboard
