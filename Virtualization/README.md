# KVM

Installing and Configuring Cockpit on CentOS 8

We're going with a leaner, more "server-like" version of CentOS 8 for the Cockpit and CLI sections. Start with a fresh CentOS 8 'minimal' installation on your computer, then proceed with the lesson. If you need a refresher, see the "How to Create the CentOS 8 Workstation Lesson Environment From Scratch" lesson.
Install 'cockpit' and the 'cockpit-machines' plugin

By default, Cockpit is installed on many Redhat distributions. You can check to see if it is installed by running:

sudo dnf list installed | egrep -i "cockpit|virt"

This will also check for virtualization packages.

    We will need to install libvirt, the cockpit, and cockpit-machines packages and any dependencies:

    sudo dnf -y install libvirt cockpit cockpit-machines

    Now would also be a good time to check for updates:

    sudo dnf -y update

    Confirm that the firewall is configured to allow Cockpit traffic to pass:

    sudo firewall-cmd --list-all

    You should see cockpit listed in services. If it is not, execute the following to allow communication:

    sudo firewall-cmd --zone=public --add-service=cockpit --permanent

    sudo firewall-cmd --reload

Configuring Permissions for Non-root Users

We need to do some work to make Cockpit work for non-root users:

    Let's add admin to the libvirt group:

    sudo usermod -a -G libvirt `id -un`

    Add admin to the libvirtdbus group:

    sudo usermod -a -G libvirtdbus `id -un`

    Add the libvirtdbus user to the libvirt group:

    sudo usermod -a -G libvirt libvirtdbus

    Add the qemu user to the libvirt group:

    sudo usermod -a -G libvirt qemu

Start Everything!

Now, let's start the cockpit.socket and libvirtd services:

    Enable and start Cockpit:

    sudo systemctl enable cockpit.socket --now

    Check the status of cockpit.socket:

    sudo systemctl status cockpit.socket

    Enable and start libvirtd:

    sudo systemctl enable libvirtd --now

    Check the status of libvirtd:

    sudo systemctl status libvirtd

Virtual Disk Storage Location

By default, virtual disks are stored in /var/lib/libvirt/images. This is in the root (/) filesystem. We don't want to fill this filesystem up, so we're going to create a directory on the /home filesystem to store virtual disks.

Create a dedicated directory for our virtual disks:

    Create the directory:

    sudo mkdir /home/Virtual_Machines

    Set the proper ownership:

    sudo chown -R qemu:libvirt /home/Virtual_Machines

    Set the proper permissions:

    sudo chmod 0770 /home/Virtual_Machines

    Confirm we have what we want:

    sudo ls -la /home

    Launch Firefox and connect to Cockpit from another computer on your network on port 9090. Use Firefox if you can, as Chrome will not accept the SSL certificate.

To enable NFS sever in the KVM host, we can do the following:

sudo dnf install nfs-utils
sudo systemctl start nfs-utils.service
sudo systemctl enable nfs-utils.service
sudo mkdir /path/to/share
sudo vim /etc/exports

In the file:
        /path/to/share iprange/subnet (rw, sync)

sudo exportfs -arv

To enable cgroup2 and podman in the host machine for running pods and containers, do: