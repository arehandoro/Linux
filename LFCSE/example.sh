#!/bin/bash

echo
echo "This is a basic bash script example."
echo
# Display user
echo "User: "`whoami`
echo "Current Directory: " $( pwd )
# echo -n "Current shell:"
# Display OS Release Info. The lsb-core package needs to be installed. Package name is "redhat-lsb-core" for CentOS.
lsb_release -d
echo
