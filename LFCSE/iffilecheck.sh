#!/bin/bash

# Declare variables
##DIRECTORY="/home/$USER/"

read -p "Enter a file path to search for: " FILE
# Evaluate
if [[ -f $FILE ]]; then
  echo "The file ($FILE) exists."
else
  echo "The file ($FILE) does NOT exist."
fi