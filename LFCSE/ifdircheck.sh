#!/bin/bash

# Declare variable
##DIRECTORY="/home/$USER/scripts/test"

read -p "Enter a directory path to search for: " DIRECTORY
# Evaluate
if [[ -d $DIRECTORY ]]; then
  echo "The directory ($DIRECTORY) exists."
else
  echo "The directory ($DIRECTORY) does not exist."
fi
