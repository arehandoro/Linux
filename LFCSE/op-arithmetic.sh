#!/bin/bash

#Reading data from the user
#clear
echo
read -p 'Enter a number for a : ' a
read -p 'Enter a number for b : ' b
echo
echo "Arithmetic operators"
echo
echo Adding $((a)) and $((b)) equals $((a + b))
echo Substracting $((a)) from $((b)) equals $((a - b))
echo Multiplying $((a)) and $((b)) equals $((a * b))
echo Dividing $((a)) by $((b)) equals $((a / b))
echo The Modulus of $((a)) and $((b)) is $((a % b))
echo Incrementing $((a)) by 1 \(++$((a))\) equals $((++a))
echo Reducing $((b)) by 1 \(--$((b))\) equals $((--b))
echo