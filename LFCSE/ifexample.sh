#!/bin/bash

FIRST=10
SECOND=20

if [[ $((FIRST)) -eq $((SECOND)) ]]; then
  echo Is $((FIRST)) equal to $((SECOND))?
  echo Yes, $((FIRST)) is equal to $((SECOND))
else
  echo Is $((FIRST)) equal to $((SECOND))?
  echo No, $((FIRST)) is NOT equal to $((SECOND))
fi
