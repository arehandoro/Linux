#!/bin/bash

TIMER=0

until [[ $TIMER -gt 5 ]]; do
  echo Count up: $TIMER
  ((TIMER++))
done
