#!/bin/bash

/usr/sbin/adduser --disabled-password --gecos "" admin
/usr/sbin/usermod -aG sudo admin
/usr/bin/cp -r ~/.ssh /home/admin
/usr/bin/chown -R admin:admin /home/admin/.ssh
/usr/bin/apt update
/usr/bin/apt upgrade -y
#/usr/bin/apt install ufw -y
#/usr/sbin/ufw allow OpenSSH
#/usr/sbin/ufw enable --force enable
