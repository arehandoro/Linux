#!/bin/bash

docker run -ti --rm \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "/home/admin/web/cloudflare.ini:/cloudflare.ini" \
    certbot/dns-cloudflare:latest \
    certonly \
    --dns-cloudflare \
    --dns-cloudflare-credentials "/cloudflare.ini" \
    -d "42beyond.space" \
    --email sysadmin@42ruling.space \
    --agree-tos \
    --server https://acme-v02.api.letsencrypt.org/directory

