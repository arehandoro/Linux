#!/bin/bash

if [ "$1" == "matrix" ]; then
    server=159668579
elif [ "$1" == "mail" ]; then
    server=124714662
elif [ "$1" == "web" ]; then
    server=153991532
else
    echo "$1 is not a known server."
fi

echo "Shutting down $server"
/snap/bin/doctl compute droplet-action power-off $server
sleep 85
echo "Resizing droplet to $2"
/snap/bin/doctl compute droplet-action resize $server --size=$2
sleep 120
echo "Starting $server"
/snap/bin/doctl compute droplet-action power-on $server
